# IDnDP

IDnDP is a project aims to combine the [IDP-Z3 logical reasoning system](https://idp-z3.be/) with [Dungeons and Dragons](https://en.wikipedia.org/wiki/Dungeons_%26_Dragons).
It is based on the intuition that the task of creating a D&D character is a type of [Configuration Problem](https://en.wikipedia.org/wiki/Configuration_design), which the IDP system excells in modeling.
While many configuration tools and websites already exist, IDnDP's novel, logic-based approach holds many advantages compared to them.

This project is still in development.
If you wish to try it out, check out [How to run](#how-to-run).
We are also happily accepting [community contributions](#how-to-contribute).


## What is IDnDP

IDnDP is an implementation of a D&D character configurations tool based on the [IDP-Z3 system](https://idp-z3.be) and its accompanying [Interactive Consultant](https://idp-z3.be/interactive_consultant.html) tool.
The IDP-Z3 system is a declarative, logic-based reasoning tool.
Due to this declarative nature, we describe the knowledge needed to build a character as-is, without specifying *how* to build a character.
Hence, we do not program a specific *order*, such as ``start with selecting a class, then a race, then a weapon, ...''.

All knowledge is contained in rules (either definitional rules or constraints/axioms).
These rules are encoded in an extension of First-Order Logic, called FO(&middot;): this makes the KB highly interpretable!
Consider the following definition of a class's spell attack modifier:

```
{
  spell_atk_modifier() = 0 <- class() in {barbarian, fighter, monk, rogue}. // These classes have no spells by default.
  spell_atk_modifier() = proficiency_bonus() + ability_modifier(charisma) <- class() in {bard, paladin, sorcerer}.
  spell_atk_modifier() = proficiency_bonus() + ability_modifier(wisdom) <- class() in {cleric, druid, ranger}.
}
```
In three rules, we say that (a) barbarian, fighter, ... have no spell attack modifier, (b) bards, paladins and sorcerers have charisma-based spell attack modifier and (c) clerics, druids and rangers have a wisdom-based spell attack modifier.


The overall approach of IDnDP has several advantages:

* Due to its declarative nature, the KB is intuitive to interpret. If any of the rules of D&D changes, as they sometimes do, adapting the KB to reflect this change is not much work compared to traditional configurators.
* The knowledge can be used *in all directions*, meaning that you do not have to start by picking a class, race, ... but can instead e.g. start by saying ``I want a sword, spell x and y, and a shield. What are my options for classes?''. In this way, you can more easily create flavorful charactors.
* The IDP system supports optimization: say that you want the max strength possible, given some previous choices. You can simply click the optimization button, and the system does the rest.
* The configurator is explainable: if you're unsure why an option isn't possible, you can simply query the system and it will respond with the relevant rules.
* It is possible to *override* specific rules, which is typically useful when, together with your DM, you decide on a rule exception.
* Homebrew rules can be added with relative ease -- implementing new classes, races, weapes, ... is all possible with just a bit of IDP knowledge.

## How to run

**Note:** we are working on hosting an online version, free to use. Until then, you need to set up your own IDP-Z3.

Follow IDP-Z3's [installation instruction](https://gitlab.com/krr/IDP-Z3) to get started with the system.
After that, run `python3 main.py`, go to `localhost:5000` and copy-paste the `idndp.idp` file into the Interactive Consultant.

## Structure of Knowledge Base

Due to its declarative nature, the structure should be intuitive to interpret (at least, after spending some time looking at the knowledge)..
TODO

## How to contribute

Everyone can feel free to contribute by extending the KB and creating merge requests.
Due to copyright reasons, we may only implement the information contained in the SRD-OGL_V5.1 pdf, also present in the repo.

For now, most classes are present, but only very basically.
Class-specific features have not really been implemented yet.

TODO: list of what's left to do

### Dev setup

It is quite a hassle to work on large knowledge bases in the Interactive Consultant's editor.
Instead, you might want to consider working in your offline editor of choice, and running the IDP-Z3 CLI once in a while, to be sure that everything still works accordingly.
Additionally, we advise the usage of [FOLint](https://github.com/larsver/folint) as a tool to avoid errors and style mistakes.
